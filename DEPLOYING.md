# Deploying

## Prerequisites

```sh
curl https://nixos.org/nix/install | sh
. /home/user/.nix-profile/etc/profile.d/nix.sh
```

## Backend (blockchain node)

```sh
git clone https://github.com/mozilla/nixpkgs-mozilla
sh nixpkgs-mozilla/rust-overlay-install.sh

git clone https://gitlab.com/goto-ru/chain/blockchain
cd blockchain
nix-shell
cargo build --release

# => target/release/go_to_cc is the resulting binary

# FIXME: generate keys
mkdir bundle && cp {target/release/go_to_cc,*.keys} bundle/

# => bundle/ can be deployed wherever
# use systemd or anything else to daemonize
# it listens on port 1488, reverse proxy for production
```

## Frontend

```sh
git clone https://gitlab.com/goto-ru/chain/web-spa-react
cd web-spa-react
nix-shell

# FIXME
cd node_modules/@goto-ru/chain-jsclient
nix-shell
cd ~/web-spa-react

yarn build

# => build/ is webserver root
```
